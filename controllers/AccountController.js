const requestJson = require('request-json');

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechumbm11ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const mLabFieldId = 'f={"_id":0}';

function getAccountsByIdV2(req, res){
  console.log("GET /apitechu/v2/accounts/:userid");
  console.log(" >> PARAMS:");
  console.log(req.params);

  var userid = req.params.userid;
  var query = 'q={"userId":' + userid + '}';
  console.log(" >> Consulta = " + query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log(" >> Cliente http creado");
  httpClient.get("account?" + query + "&" + mLabFieldId + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      // console.log(resMLab);
      if (err) {
        var respuesta = {
          "msg": "Error obteniendo cuentas"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var respuesta = body;
        } else {
          var respuesta = {
            "msg": "no hay cuentas"
          }
          res.status(404);
        }
      }

      res.send(respuesta);
    }
  );
}

module.exports.getAccountsByIdV2 = getAccountsByIdV2;
