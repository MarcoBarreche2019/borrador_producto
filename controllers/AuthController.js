const requestJson = require('request-json');

const io = require('../io'); // representa nuestra librería de acceso a ficheros (io)
const crypt = require('../crypt'); // representa nuestra librería de criptografía

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechumbm11ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const mLabFieldId = 'f={"_id":0}';

function loginV2(req, res){
  console.log("POST /apitechu/v2/login");
  console.log(" >> BODY:");
  console.log(req.body);

  var email = req.body.email;
  var query = 'q={"email":"' + email + '"}';
  console.log(" >> Consulta = " + query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log(" >> Cliente http creado");
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      // console.log(resMLab);
      console.log(body);
      var respuesta = {};
      if (body.length > 0) {
        console.log("comparar pwd introducida " + req.body.password + " y almacenada " + body[0].password);
        var logged = crypt.checkPassword(req.body.password, body[0].password);
        var msg = logged ? "Usuario validado" : "Usuario no validado";
        if (logged){
          respuesta.id = body[0].id;
          respuesta.msg = msg;

          // body[0].logged = logged;
          // httpClient.put("user?" + query + "&" + mLabAPIKey, body[0],

          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(err, resMLab, body) {
              console.log(body);
              res.send(respuesta);
            }
          );
        } else {
          respuesta.msg = msg;
          res.status(401).send(respuesta);
        }
      } else {
        var msg = "Usuario no validado";
        respuesta.msg = msg;
        res.status(401).send(respuesta);
      }
    }
  );
}

function logoutV2(req, res){
  console.log("POST /apitechu/v2/logout/:id");
  console.log(" >> PARAMS:");
  console.log(req.params);

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';
  console.log(" >> Consulta = " + query);
  var userLogged = [];

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log(" >> Cliente http creado");
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      //console.log(resMLab);
      console.log(body);
      var respuesta = {};
      if (body.length > 0) {
        console.log("comparar si usuario logado");
        var msg = body[0].logged ? "Usuario deslogado" : "Usuario no logado";
        if (body[0].logged){
          respuesta.id = body[0].id;
          respuesta.msg = msg;

          // delete body[0].logged;
          // httpClient.put("user?" + query + "&" + mLabAPIKey, body[0],

          var putBody = '{"$unset":{"logged":""}}';
          httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
            function(err, resMLab, body) {
              console.log(body);
              res.send(respuesta);
            }
          );
        } else {
          respuesta.msg = msg;
          res.send(respuesta);
        }
      } else {
        var msg = "Usuario no logado";
        respuesta.msg = msg;
        // res.status(404).send(respuesta);
        res.send(respuesta);
      }
    }
  );
}


function loginV1(req, res){
  console.log("POST /apitechu/v1/login");
  console.log(" >> BODY:");
  console.log(req.body);

  var response = {};

  var users = io.readUserDataFromFile();
  console.log("users = " + users.length);
  var i = -1;
  var logged = false;

  i = findUserDataByEmail(users, req.body.email);

  if (i >= 0  ) {
    logged = validateUser(users[i], req.body.password);
  }

  var msg = logged ? "Usuario validado" : (i >= 0? "Usuario no validado" : "Usuario no encontrado");
  response.msg = msg;

  if (logged){
    response.id = users[i].id;
    var user = users[i];
    user.logged = logged;
    users.splice(i, 1, user);
    io.writeUserDataToFile(users);
  }

  console.log("login de usuario: " + msg);
  res.send(response);
}

function logoutV1(req, res){
  console.log("POST /apitechu/v1/logout/:id");
  console.log(" >> PARAMS:");
  console.log(req.params);

  var response = {};

  var users = io.readUserDataFromFile();
  console.log("users = " + users.length);
  var i = -1;
  var logged = false;

  i = findUserDataById(users, req.params.id);

  if (i >= 0  ) {
    console.log(" user = " + users[i]);
    logged = users[i].logged;
  }

  var msg = logged ? "Usuario deslogado" : (i >= 0? "Usuario no logado" : "Usuario no encontrado");
  response.msg = msg;

  if (logged){
    response.id = users[i].id;
    var user = users[i];
    delete user.logged;
    users.splice(i, 1, user);
    io.writeUserDataToFile(users);
  }

  console.log("logout de usuario: " + msg);
  res.send(response);
}

// Método de búsqueda (for)
function findUserDataByEmail(users,emailId){
  var index = -1;
  console.log(" >> findUserDataByEmail: " + emailId);

  for (i=0; i<users.length; i++)  {
    var user = users[i];
    console.log("user = " + JSON.stringify(user));

    if (user.email == emailId) {
      console.log("encontrado id = " + emailId + " en posición " + i);
      index = i;
      break;
    }
  }

  return index;
}

// Método de búsqueda (Array.findIndex)
function findUserDataById(users,userId){
  var index = -1;

  index = users.findIndex(function (element) {
      console.log (element);
      return element.id == userId;
    }
  );

  return index;
}

// Método de validación
function validateUser(user,pwd){
  var validate = false;
  console.log(" >> validateUser: user = " + JSON.stringify(user));
  console.log(" >> validateUser: pwd = " + pwd);

  if (user.password == pwd) {
    console.log("passord OK");
    validate = true;
  } else {
    console.log("passord KO");
  }

  return validate;
}

module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;

module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
