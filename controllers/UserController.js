const requestJson = require('request-json');

const io = require('../io'); // representa nuestra librería de acceso a ficheros (io)
const crypt = require('../crypt'); // representa nuestra librería de criptografía

const baseMLabURL = "https://api.mlab.com/api/1/databases/apitechumbm11ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;
const mLabFieldId = 'f={"_id":0}';

function getUsersV2(req, res){
  console.log("GET /apitechu/v2/users");
  console.log(" >> QUERY STRING:");
  console.log(req.query);
  var params = req.query;

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log(" >> Cliente http creado");
  httpClient.get("user?" + mLabFieldId + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      // console.log(resMLab);

      var respuesta = !err ? body : {"msg":"Error obteniendo usuarios"};
      // var users = io.readUserDataFromFile();
      // var respuesta = {};
      //
      // if (params.$count=="true") {
      //   console.log("Nº registros users = " + users.length);
      //   respuesta.count = users.length;
      // }
      //
      // respuesta.users = params.$top ? respuesta.users = users.slice(0, params.$top) : users;
      //

      res.send(respuesta);
    }
  );
}

function getUsersByIdV2(req, res){
  console.log("GET /apitechu/v2/users/:id");
  console.log(" >> PARAMS:");
  console.log(req.params);

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';
  console.log(" >> Consulta = " + query);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log(" >> Cliente http creado");
  httpClient.get("user?" + query + "&" + mLabFieldId + "&" + mLabAPIKey,
    function(err, resMLab, body) {
      // console.log(resMLab);
      if (err) {
        var respuesta = {
          "msg": "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if (body.length > 0) {
          var respuesta = body[0];
        } else {
          var respuesta = {
            "msg": "Usuario no encontrado"
          }
          res.status(404);
        }
      }

      res.send(respuesta);
    }
  );
}

function createUserV2(req, res){
  console.log("POST /apitechu/v2/users");
  console.log(" >> BODY:");
  console.log(req.body);

  var newUser = {
    "id": req.body.id,
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password": crypt.hash(req.body.password)
  };
  console.log(" >> newUser:");
  console.log(newUser);

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log(" >> Cliente http creado");
  httpClient.post("user?" + mLabAPIKey, newUser,
    function(err, resMLab, body) {
      console.log("Usuario guardado");

      res.status(201).send({"msg":"Usuario creado"});
    }
  );
}

function deleteUserByIdV2(req, res){
  console.log("DELETE /apitechu/v2/users/:id");
  console.log(" >> PARAMS:");
  console.log(req.params);

  var id = req.params.id;
  var query = 'q={"id":' + id + '}';
  console.log(" >> Consulta = " + query);
  var userDeleted = [];

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log(" >> Cliente http creado");
  httpClient.put("user?" + query + "&" + mLabAPIKey, userDeleted,
    function(err, resMLab, body) {
      //console.log(resMLab);
      console.log(body);
      if (err) {
        var respuesta = {
          "msg": "Error obteniendo usuario"
        }
        res.status(500);
      } else {
        if(body.removed>0) {
          var respuesta = {
            "msg": "Usuario borrado"
          }
        } else {
          var respuesta = {
            "msg": "Usuario no encontrado"
          }
          res.status(404);
        }
      }
      res.send(respuesta);
    }
  );
}

function getUsersV1(req, res){
  console.log("GET /apitechu/v1/users");
  console.log(" >> QUERY STRING:");
  console.log(req.query);
  var params = req.query;

  // res.sendFile('usuarios.json', {root: __dirname})
  var users = io.readUserDataFromFile();
  var respuesta = {};

  if (params.$count=="true") {
    console.log("Nº registros users = " + users.length);
    respuesta.count = users.length;
  }

  respuesta.users = params.$top ? respuesta.users = users.slice(0, params.$top) : users;
  // if(params.$top) {
  //   console.log("Lista contiene " + params.$top + " registros");
  //   // console.log("elementos = " + users.slice(0, params.$top));
  //   respuesta.users = users.slice(0, params.$top);
  // } else {
  //   console.log("Lista completa de registros");
  //   respuesta.users = users;
  // }

  res.send(respuesta);
}

function createUserV1(req, res){
  console.log("POST /apitechu/v1/users");
  console.log(" >> BODY:");
  console.log(req.body);

  var newUser = {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email
  };

  var users = io.readUserDataFromFile();
  users.push(newUser);
  console.log("Usuario añadido al array");
  io.writeUserDataToFile(users);

  console.log("Proceso de creación de usuario terminado");
  res.send({"msg":"Usuario creado"});
}

function deleteUserV1(req, res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log(" >> PARAMS:");
  console.log(req.params);

  var users = io.readUserDataFromFile();
  console.log("users = " + users.length);
  var i = -1;

  // // Método 1 de búsqueda (for)
  //i = findUserDataAlt1(users, req.params.id);

  // // Método 2 de búsqueda (array.forEach)
  // i = findUserDataAlt2(users, req.params.id);

  // // Método 3 de búsqueda (for in)
  // i = findUserDataAlt3(users, req.params.id);

  // // Método 4 de búsqueda (for of)
  // i = findUserDataAlt4(users, req.params.id);

  // // Método 5 de búsqueda (Array.findIndex)
  i = findUserDataAlt5(users, req.params.id);

  if (i >= 0  ) {
    console.log("encontrado id = " + req.params.id + " en posición " + i);
    users.splice(i, 1);
    io.writeUserDataToFile(users);
  }
  var msg = i>=0 ? "Usuario borrado" : "Usuario no encontrado";
  console.log("Proceso de borrado de usuario terminado: " + msg);
  res.send({"msg":msg});
}

// Método 1 de búsqueda (for)
function findUserDataAlt1(users,userId){
  var index = -1;

  for (i=0; i<users.length; i++)  {
    var user = users[i];
    console.log("user = " + JSON.stringify(user));

    if (user.id == userId) {
      console.log("encontrado id = " + userId + " en posición " + i);
      index = i;
      break;
    }
  }

  return index;
}

// Método 2 de búsqueda (array.forEach)
function findUserDataAlt2(users,userId){
  var index = -1;

  users.forEach(function(user,i) {
    console.log(user);
    if (user.id == userId) {
      console.log("encontrado id = " + userId + " en posición " + i);
      index = i;
    }
  });

  return index;
}


// Método 3 de búsqueda (for in)
function findUserDataAlt3(users,userId){
  var index = -1;

  for (const indice in users) {
    console.log(indice);
    console.log(users[indice]);
    if (users[indice].id == userId) {
      console.log("encontrado id = " + userId + " en posición " + indice);
      index = indice;
      break;
    }
  }

  return index;
}

// Método 4 de búsqueda (for of)
function findUserDataAlt4(users,userId){
  var index = -1;

  for (let user of users) {
    console.log(user);
    if (user.id == req.params.id) {
      console.log("encontrado id = " + req.params.id + " en posición " + users.indexOf(user));
      encontrado = true;
      index = users.indexOf(user);
      break;
    }
  }

  return index;
}

// Método 5 de búsqueda (Array.findIndex)
function findUserDataAlt5(users,userId){
  var index = -1;

  index = users.findIndex(function (element) {
      console.log (element);
      return element.id == userId;
    }
  );

  return index;
}

module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUserV1 = deleteUserV1;

module.exports.getUsersV2 = getUsersV2;
module.exports.getUsersByIdV2 = getUsersByIdV2;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserByIdV2 = deleteUserByIdV2;
