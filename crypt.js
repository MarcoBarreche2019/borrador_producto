const bcrypt = require('bcrypt');

function hash(data) {
  console.log("Hashing data");
  return bcrypt.hashSync(data, 10);
}

function checkPassword(pwdUserPlainText,pwdUserHashed) {
  console.log("Checking password");
  return bcrypt.compareSync(pwdUserPlainText, pwdUserHashed); // true
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
