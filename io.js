const fs = require('fs');

const usuarios = './usuarios.json';

function readUserDataFromFile(){
  var users = require(usuarios);
  return users;
}

function writeUserDataToFile(data){
  var jsonUserData = JSON.stringify(data);

  fs.writeFile(usuarios, jsonUserData, "utf8",
    function(err){
      if (err) {
        console.log(err);
      } else {
        console.log ("Datos almacenados en fichero de usuario");
      }
    }
  );
}

module.exports.readUserDataFromFile = readUserDataFromFile;
module.exports.writeUserDataToFile = writeUserDataToFile;
