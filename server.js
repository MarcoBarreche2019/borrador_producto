console.log("inicializamos la aplicación");

require('dotenv').config();

const express = require('express'); // representa nuestro framework que usaremos en el proyecto
const app = express();  // inicializamos el framework de express
app.use(express.json());  // indicamos al framework de express que procese las peticiones del body como json

const port = process.env.PORT || 3000; // definimos el puerto en el que va a estar escuchando nuestra aplicación node.js

const userController = require('./controllers/UserController');
const authController = require('./controllers/AuthController');
const accountController = require('./controllers/AccountController');

var enableCORS = function (req, res, next) {
  res.set("Access-Control-Allow-Origin", "*");
  res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
  res.set("Access-Control-Allow-Headers", "Content-Type");

  next();
}

app.use(enableCORS);

app.listen(port);
console.log("API escuchando de nuevo en el puerto " + port);

app.get("/apitechu/v1/users", userController.getUsersV1);
app.post("/apitechu/v1/users", userController.createUserV1);
app.delete("/apitechu/v1/users/:id", userController.deleteUserV1);

app.get("/apitechu/v2/users", userController.getUsersV2);
app.get("/apitechu/v2/users/:id", userController.getUsersByIdV2);
app.post("/apitechu/v2/users", userController.createUserV2);
app.delete("/apitechu/v2/users/:id", userController.deleteUserByIdV2);

app.post("/apitechu/v1/login", authController.loginV1);
app.post("/apitechu/v1/logout/:id", authController.logoutV1);

app.post("/apitechu/v2/login", authController.loginV2);
app.post("/apitechu/v2/logout/:id", authController.logoutV2);

app.get("/apitechu/v2/accounts/:userid", accountController.getAccountsByIdV2);

app.get("/apitechu/v1/hello",
  function(req, res){
    console.log("GET /apitechu/v1/hello");
    //res.send('{"msg":"Hola desde APITechU"}');
    res.send({"msg":"Hola desde APITechU"});
  }
);
//
// app.post("/apitechu/v1/monsters/:value1/:value2",
//   function(req, res){
//     console.log("POST /apitechu/v1/monsters/:value1/:value2");
//     console.log(" >> HEADERS:");
//     console.log(req.headers);
//     console.log(" >> PARAMS:");
//     console.log(req.params);
//     console.log(" >> QUERY STRING:");
//     console.log(req.query);
//     console.log(" >> BODY:");
//     console.log(req.body);
//
//     res.send({"res":"OK"});
//   }
// );
