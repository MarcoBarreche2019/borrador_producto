const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();

describe("First test",
  function() {
    it("Test that DuckDuckGo works", function(done) {
      chai.request('http://duckduckgo.com')
      // chai.request('https://developer.mozilla.org/en_US/adsfdsdfkfjs')
        .get('/')
        .end(
          function (err, res) {
            console.log("Request finished");
            // console.log(res);
            // console.log(err);
            res.should.have.status(200);
            done();
          }
        )
    })
  }
)

describe("Test de API V1 de Users",
  function() {
    it("Prueba que la API responde", function(done) {
      chai.request('http://localhost:3000')
        .get('/apitechu/v1/hello')
        .end(
          function (err, res) {
            console.log("Request finished");
            res.should.have.status(200);
            res.body.msg.should.be.eql("Hola desde APITechU");
            done();
          }
        )
      }
    ),
    it("Prueba que la API devuelve una lista de usuarios", function(done) {
      chai.request('http://localhost:3000')
        .get('/apitechu/v1/users?$count=true')
        .end(
          function (err, res) {
            console.log("Request finished");
            res.should.have.status(200);
            res.body.users.should.be.a("array");
            res.body.should.have.property('count');
            for (user of res.body.users) {
              user.should.have.property('first_name');
              user.should.have.property('email');
            }
            done();
          }
        )
      }
    )
  }
)
